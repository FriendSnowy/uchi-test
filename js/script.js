var canvas = document.getElementById("arrow");
var ctx = canvas.getContext("2d");

var randomInt = function(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    return Math.round(rand);
}
var calcPosition = function(position) {
    var startInPx = 35;
    var cmInPx = 39;

    return startInPx + cmInPx * position;
}

var firstNumber = randomInt(6, 9);
var result = randomInt(11, 14);

var img = new Image();
img.onload = function() {
    document.getElementById("image").getContext("2d").drawImage(this, 0, canvas.height-100);
}
img.src="img/sprite-min.png";

ctx.makeCurve = function(from, to, color="#000000", height = canvas.height) { 
    var startPos = calcPosition(from);
    var endPos = calcPosition(to);
    
    this.strokeStyle=color;
    this.beginPath();
    this.moveTo(startPos, height-80);
    this.bezierCurveTo(
        startPos, height-80, 
        (startPos+endPos)/2, height - 15 * (to-from) - 80, 
        endPos, height-80
    );
    this.stroke();
}

var makeInput = function (from, to, checkingElement, onSuccess, height = canvas.height) {
    var startPos = calcPosition(from);
    var endPos = calcPosition(to);
    var input = document.createElement("input");

    input.type = "number";
    input.style.left = ((startPos+endPos)/2-16) + "px";
    input.style.top = height - 7 *(to-from) + "px";

    input.classList.add("number-input");
    document.body.appendChild(input);
    input.focus();

    input.oninput = function() {
        if (parseInt(input.value) != parseInt(checkingElement.innerText)) {
            input.classList.add("wrong");
            checkingElement.classList.add("active");
        }
        else {
            if (checkingElement.classList.contains("active"))
                checkingElement.classList.remove("active");
            var span = document.createElement("span");
            span.innerText = input.value;
            span.classList.add("number-result");
            span.style.left = ((startPos+endPos)/2 + 6) + "px";
            span.style.top = height - 4 * (to-from) + "px";
            document.body.removeChild(input);
            document.body.appendChild(span);

            onSuccess();
        }
    }
}

var firstNumberElement = document.getElementById("number-1");
var secondNumberElement = document.getElementById("number-2");

firstNumberElement.innerText = firstNumber;
secondNumberElement.innerText = result-firstNumber;

ctx.makeCurve(0, firstNumber, "green");
makeInput(0, firstNumber, firstNumberElement, function() {
    ctx.makeCurve(firstNumber, result, "green");
    makeInput(firstNumber, result, secondNumberElement, function() {
        var resultElement = document.getElementById("result");
        resultElement.innerText = "";
        
        var resultInput = document.createElement("input");
        resultInput.classList.add("result-input");
        resultInput.type = "number";
        resultElement.appendChild(resultInput);
        resultInput.focus();

        resultInput.oninput = function() {
            if (parseInt(resultInput.value) === result) {
                resultInput.remove();
                resultElement.innerText = result;
            }
            else
                resultInput.classList.add("wrong");
        }
    })
});